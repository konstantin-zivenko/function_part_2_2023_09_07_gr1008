def outer_func():
    var = 100

    def inner_func():
        print(f"Printing var from inner_func(): {var}")
        print(f"Printing another_var from inner_func(): {another_var}")

    another_var = 200
    inner_func()
    print(f"Printing var from outer_func(): {var}")


if __name__ == "__main__":
    ...

