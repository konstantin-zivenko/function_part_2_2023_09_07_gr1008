def factorial(n):
    print(f"factorial called with n = {n}")
    result =  1 if n <= 1 else n * factorial(n - 1)
    print(f"-----> factorial({n}) returns {result}")
    return result

if __name__ == "__main__":
    factorial(4)
